module namespace _ = "html5book/media";
import module namespace u = "html5book/utils" at "utils.xqm";

declare function _:retrieve($node as node(), $src as xs:string)
{
	let $db := try { db:name($node) } catch * { () }
	let $uri := resolve-uri($src, u:base-uri($node))
	return u:retrieve($uri)
};

declare function _:find($node as node()*, $options as map(*)) as element(media)*
{
	let $target := map:get($options, "target")
	let $db := try { db:name($node) } catch * { () }
	let $uri := base-uri($node)
	return
	for $m in $node//(*:audio|*:video|*:img) return
		for $src in ($m/*:source, $m)[@src and u:filter(., $target)] return
			<media type="object">
				{
					if ($db) then
						<node>{$db || ":" || db:node-id($src)}</node>
					else ()
				}
				{ if ($m/@id) then element id {$m/@id/string()} else () }
				<src>{$src/@src/string()}</src>
				<type>
				{
					if ($src/@type)
					then $src/@type/string()
					else u:mime-type($src/@src)
				}
				</type>
				{
					let $exists :=
						if ($db)
						then db:exists($db, $src/@src)
						else file:exists(resolve-uri($src/@src, $uri))
					return
						if ($exists)
						then ()
						else <missing type="boolean">true</missing>
				}
			</media>
};

declare function _:to-json($media as element(media)) as element(json)
{
	<json type="object">{$media/node()}</json>
};

declare function _:to-json-array($media as element(media)*) as element(json)
{
	copy $json := <json type="array">{$media}</json>
	modify (
		for $m in $json/media return
			rename node $m as "_"
	)
	return $json
};
