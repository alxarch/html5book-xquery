module namespace _ = "graphicsmagick";

declare variable $_:executable as xs:string external := "gm";

declare function _:execute($args as xs:string*) as empty-sequence()
{
	proc:execute($_:executable, $args)/ignore-output
};

declare %private function _:mkdirp($path as xs:string) as empty-sequence()
{
	let $dir := if (ends-with($path, "/")) then $path else file:parent($path)
	return
	if (file:is-dir($dir))
	then ()
	else file:create-dir($dir)

};

declare function _:thumbnail($img as xs:string, $thumb as xs:string, $size as xs:string) as empty-sequence()
{
	let $box := replace($size, "^(\d+x\d+).*$", '$1')
	let $size := if(matches($size, "^\d+$")) then $size || 'x' else $size
	let $args := (
    	'convert',
        if($box) 
            then ('-size', $box) 
            else (),
        $img,
        if($size eq 'full') 
            then () 
            else ('-resize', $size),

        (: Center the thumbnail within it's box. :)
        if($box eq '') then () else (
            '-background', 'none',
            '-compose', 'Copy',
            '-gravity', 'Center',
            '-extent', $size
        ),
        $thumb
    )

    return (
    	_:mkdirp($thumb),
    	_:execute($args)
    )
};

declare function _:size($img as xs:string) as item(){
    let $ident := proc:system('gm', ('identify', $img))
    let $size := tokenize($ident, ' ')[3]
    let $size := substring-before($size, '+')
    let $size := tokenize($size, 'x')
    return
    <size>
        <w>{$size[1]}</w>
        <h>{$size[2]}</h>
    </size>
};

declare function _:crop($img as xs:string, $dest as xs:string, $params as item())
{
    let $size := _:size($img)

    let $w := floor($params/w * $size/w)
    let $h := floor($params/h * $size/h)
    let $x := floor($params/x * $size/w)
    let $y := floor($params/y * $size/h)

    let $d := math:sqrt($w * $w + $h * $h)

    let $g := $w || 'x' || $h || '+' || $x || '+' || $y

    let $border := floor($d * $params/border * 0.01)
 
    let $args := (
        'convert',

        $img,

        ('-crop', $g),

        if($params/trim eq 'true') then '-trim' else (),

        if($params/border eq '0') 
            then () 
            else ('-bordercolor', 'none', '-border', $border || ''),

        $dest
    )
    
    return (
        _:mkdirp($dest), 
    	_:execute($args)
    )
    
};

declare function _:valid-size($size as xs:string) as xs:boolean
{
    matches($size, '^(\d+(x\d+[!><@]?)?|full)$')
};
