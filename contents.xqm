module namespace _ = "html5book/contents";

import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";
import module namespace term = "html5book/terms" at "terms.xqm";
import module namespace roles = "html5book/roles" at "roles.xqm";
import module namespace title = "html5book/title" at "title.xqm";
import module namespace label = "html5book/label" at "label.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";

declare %private function _:previous($node as node(), $roles as xs:string*, $target as xs:string?) as node()?
{
	head($node/preceding-sibling::*:section[@role = $roles])
};

declare %private function _:next($node as node(), $roles as xs:string*, $target as xs:string?) as node()?
{
	head($node/following-sibling::*:section[@role = $roles])
};

declare %private function _:guess-depth($node as node(), $roles as xs:string*) as xs:integer
{
	count($node/ancestor::*[@role = $roles]) + 1
};

declare function _:items($node as node()) as element(item)*
{
	_:items($node, map {})
};

declare function _:items($node as node(), $options as map(*)) as element(item)*
{
	let $target := map:get($options, "target")
	let $roles := if (map:contains($options , "roles"))
		then map:get($options, "roles")
		else roles:book(root($node))[type = "section"]/id/string()
	let $depth := if (map:contains($options, "depth"))
		then map:get($options, "depth")
		else _:guess-depth($node, $roles)
	let $max-depth := head((map:get($options, "max-depth"), 9999))
	return _:items($node, $roles, $target, $depth, $max-depth)
};

declare function _:items(
	$node as node(),
	$roles as xs:string*,
	$target as xs:string?,
	$depth as xs:integer,
	$max-depth as xs:integer) as element(item)*
{
	for $c in $node/*:section[@role= $roles and utils:filter(., $target)] return
		<item type="object">
			<id>{$c/@id/data()}</id>
			<type>{$c/@role/data()}</type>
			<label>{label:make($c)}</label>
			<title>{title:make($c)}</title>
			<depth type="number">{ $depth }</depth>
			<terms type="array">
			{
				term:ref-ids($c, map {
					"target": $target
				}) ! <_ type="string">{ . }</_>
			}
			</terms>

			<contents type="array">
			{
				if ($depth gt $max-depth)
				then ()
				else _:items($c, $roles, $target, $depth + 1, $max-depth)
			}
			</contents>
		</item>
};

declare function _:to-json($items as element()*) as element(json)
{
	copy $json := <json type="array">{$items}</json>
	modify (
		for $item in $json//item
			let $next := head($item/following-sibling::*)/id/string()
			let $prev := $item/preceding-sibling::*[last()]/id/string()
			return (
				rename node $item as "_",
				if ($next) then insert node element next {$next} into $item else (), 
				if ($prev) then insert node element prev {$prev} into $item else ()
			),
		delete node $json//item/*[. = ""]
	)
	return $json
};

declare function _:book($book as document-node()) as element(item)*
{
	_:book($book, map {})
};


declare function _:book($book as document-node(), $options as map(*)) as element(item)*
{
	_:items($book//*:body, $options)
};
