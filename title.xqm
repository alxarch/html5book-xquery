module namespace _ = "html5book/title";

import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";
import module namespace label = "html5book/label" at "label.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";

declare variable $_:drop-elements as xs:string* := (
	$html5:document-metadata-elements,
	$html5:media-elements,
	$html5:widget-elements,
	$html5:form-elements,
	$html5:table-elements,
	$html5:list-elements,
	$html5:scripting-elements,
	$html5:embedded-content-elements,
	$html5:deprecated-elements
);

(:~
 : Elements to expand when searching for title node.
 :)
declare variable $_:expand-elements as xs:string* := (
	"header", "footer", "div"
);

declare variable $_:explode-elements as xs:string* := (
	$html5:sectioning-content-elements,
	$html5:heading-content-elements,
	$html5:text-content-elements,
	"dfn"
)[not(. = $_:drop-elements)];

declare function _:html($nodes as node()*) as node()*
{
	typeswitch ($nodes)
		case attribute() return text {$nodes/data()}
		default return
	copy $cp := <_>{$nodes}</_>
	modify (
		for $node in $cp//*[local-name(.) = $_:explode-elements] return
			replace node $node with $node/node(),
		delete nodes (
			$cp//@*,
			label:node($cp),
			$cp//comment(),
			$cp//*:a[@rel = ("endnote", "footnote", "sidenote", "page")],
			$cp//*[local-name(.) = $_:drop-elements],
			$cp//*:math[@display="block"]
		)
	)
	return $cp/node()
};


(:~
 : Find the tile node.
 :
 : This function works properly for html5 outlines.
 :)
declare function _:node-alt($node as node()?) as node()?
{
	if (empty($node)) then () else
	if ($node/@title) then $node/@title else
	if ($node/@role = "title") then $node else
	let $name := local-name($node)
	return switch($name)
		case "h1"
		case "h2"
		case "h3"
		case "h4"
		case "h5"
		case "h6"
		case "title"
		case "figcaption"
		case "caption"
			return $node
		case "table"
			return head($node/*:caption)
		case "figure"
			return head($node/*:figcaption)
		case "body"
			return head($node/root()//*:title)
		default
			return head(utils:expand($node/*, $_:expand-elements) ! _:node(.))
};


(:~
 : Find the tile node.
 :
 : This function respects the heading rank so it 
 : does not work properly for html5 outlines.
 :)
declare function _:node($node as node()?) as node()?
{
	if (empty($node)) then () else
	if ($node/@title) then $node/@title else
	if (html5:heading($node) or $node[@role="title"]) then $node else
	let $elements := utils:expand(($node/*), $_:expand-elements)
	return
	head((
		$elements/self::*:h1,
		$elements/self::*:h2,
		$elements/self::*:h3,
		$elements/self::*:h4,
		$elements/self::*:h5,
		$elements/self::*:h6,
		$elements[@role = "title"],
		$node/self::*:figure/*:figcaption,
		$node/self::*:body/root()//*:title 
	))
};

declare function _:make-alt($node as node()) as node()*
{
	html5:normalize-space(_:html(_:node-alt($node)/node()))
};

declare function _:make($node as node()) as node()*
{
	html5:normalize-space(_:html(_:node($node)/node()))
};
