module namespace _ = "html5book/excerpt";

import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";
import module namespace str = "utils/string" at "modules/utils/string.xqm";

(:~
 : Modify html content to be used for excerpts
 :
 : Discards unneeded elements (embedded content, tables, scripts etc.),
 : removes all attributes, removes note links and
 : reduces all content to phrasing content.
 :)
declare function _:html($nodes as node()*) as node()*
{
	copy $html := <html>{$nodes}</html>
	modify (
		delete node (
			$html//@*,
			$html//*[local-name() = (
				"footer", "header", "figcaption",
				$html5:heading-content-elements,
				$html5:document-metadata-elements,
				$html5:scripting-elements,
				$html5:widget-elements,
				$html5:deprecated-elements,
				$html5:embedded-content-elements,
				$html5:form-elements,
				$html5:table-elements)]
				except $html//*:math[not(@display="block")],
			$html//*:a[@rel = ("endnote", "footnote", "sidenote")]
		)
	)
	return html5:reduce-to-phrasing-content($html/node())
};

(:~
 : Extract an excerpt from a series of nodes.
 :)
declare function _:make($nodes as node()*, $size as xs:integer) as node()*
{
	if ($size le 0) then () else
		let $content := _:html($nodes)
		return _:tail(<result/>, head($content), tail($content), $size)
};


(:~
 : Tail recursion to extract the excerpt.
 :)
declare %private function _:tail(
	$result as element(result),
	$node as node()?,
	$tail as node()*, 
	$max-size as xs:integer) as node()*
{
	if (empty($node)) then $result/node() else
	let $size := string-length($result)
	return if ($size ge $max-size) then $result/node() else
	let $remaining := $max-size - $size
	let $append :=
		typeswitch ($node)
			case text() return
				text {str:excerpt($node, $remaining)}
			case element() return
				let $nodes := $node/node()
				return element {name($node)} {
					_:tail(<result/>, head($nodes), tail($nodes), $remaining)
					}
			default return
				$node
	let $next := element result {$result/node(), $append}
	return _:tail($next, head($tail), tail($tail), $remaining)
};
