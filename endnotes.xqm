module namespace _ = "html5book/endnotes";

declare updating function _:fix($html as node())
{
	for $en at $i in $html//*:a[@rel="endnote" and normalize-space(.) eq ""]
		return
		insert node text {$i} as first into $en
};
