module namespace _ = "html5book/label";
import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";

declare variable $_:endings as xs:string* := (
	"--", "-", "‒", "–", "—", ":"
);

declare function _:is-label($node as node()) as xs:boolean
{
	local-name($node) = "label" or $node/@role = "label"
};

declare function _:strip($nodes as node()*) as node()*
{
	$nodes[not(_:is-label(.))]
};

declare variable $_:expand-elements as xs:string* := (
	$html5:inline-text-elements,
	$html5:heading-content-elements,
	"header", "div", "figcaption", "caption", "footer");

declare function _:expand($elements as element()*) as element()*
{
	for $el in $elements
		let $name := local-name($el)
		return
		if ($el/@role = "label" or $name = "label")
		then $el
		else if ($name = $_:expand-elements)
		then _:expand($el/*)
		else ()
};

declare function _:node($node as node()?) as node()?
{
	head($node/* ! (if (@role="title") then _:expand(./*) else _:expand(.)))
};

declare function _:make($node as node()?) as xs:string?
{
	let $node := 
		if ($node/@data-label)
		then $node/@data-label
		else _:node($node)
	return if (empty($node)) then () else
	let $txt := normalize-space($node)
	let $parts := tokenize($txt, " ")
	let $total := count($parts)
	return
	if ($parts[$total] = $_:endings)
	then string-join(subsequence($parts, $total - 1), " ")
	else $txt
};
