module namespace _ = "html5book/utils";

import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";
import module namespace roles = "html5book/roles" at "roles.xqm";

declare function _:closest($node as node(), $names as xs:string*) as element()?
{
	head($node/ancestor-or-self::*[local-name(.) = $names])
};

(:~
 : Convert a location map to element.
 :)
declare function _:location($node as node()) as element(location)
{
	_:location($node, roles:book(root($node)))
};

declare function _:location($node as node(), $roles as element(role)*) as element(location)
{
	let $location-map := _:location-map($node, $roles)
	return
	<location type="object">
	{
		map:for-each($location-map, function ($key, $value) {element {$key} {$value}})
	}
	</location>
};

(:~
 : Finds the location for a specific node
 :)
declare function _:location-map($node as node()) as map(*)*
{
	_:location-map($node, roles:book(root($node)))
};

declare function _:location-map($node as node(), $roles as element(role)*) as map(*)*
{
	let $section-roles := $roles[type="section"]/id
	let $ancestors := $node/ancestor-or-self::*:section[@role = $section-roles]
	return map:merge((
		map:entry("__root", head($ancestors)/@id/string()),
		$ancestors ! map:entry(@role, @id/string())))
};

declare function _:as-json($items as item()*) as element(json) {
	typeswitch ($items)
		case element(json) return
			$items
		case element(json)+ return
			<json type="array">{$items ! <_>{attribute(), node()}</_>}</json>
		case empty-sequence() return
			<json type="array"/>
		default return
			<json type="null"/>
};

declare function _:strip-ns($nodes as node()*) as node()*
{
  for $n in $nodes return
    typeswitch($n)
      case element() return
        element {QName("", local-name($n))} {_:strip-ns(($n/@*, $n/node()))}
      case document-node() return
        document {_:strip-ns($n/node())}
      case attribute() return
        attribute {local-name($n)} {$n/string()}
      default return
        $n
};

declare function _:find($path as xs:string) as xs:string*
{
  if (ends-with($path, "/")) then
    file:list($path) ! _:find($path || .)
  else
    $path
};

declare function _:zipball($dir) as xs:base64Binary
{
	if (file:is-dir($dir)) then
		let $files := _:find($dir)
		return archive:create(
			$files ! substring-after(., $dir),
			$files ! file:read-binary(.))
	else
		error(xs:QName("_:no-dir"), "Path " || $dir || " is not a direectory.")
};

declare function _:timestamp($precision as xs:string) as xs:string
{
	let $timestamp := xs:string(current-dateTime())
	let $timestamp := switch ($precision)
		case "year" return substring-before($timestamp, "-")
		case "month" return replace($timestamp, "(\d{4}-\d{2}).*", "$1")
		case "day" return substring-before($timestamp, "T")
		case "hour" return replace($timestamp, "(.*T\d+).*", "$1")
		case "min" return replace($timestamp, "(.*T\d+:\d+).*", "$1")
		case "sec" return substring-before($timestamp, ".")
		default return replace($timestamp, "(.*)[\+\-].*", "$1")
	let $timestamp := replace($timestamp, "[-:]", ".")
	let $timestamp := replace($timestamp, "T", "-")
	return $timestamp
};

declare function _:mime-type($file  as xs:string) as xs:string {
	(: This actually justs checks the extension :)
	Q{java:org.basex.io.MimeTypes}get($file)
};

(:~
 : Filters a node for the specified target.
 :)
declare function _:filter($node as node(), $target as xs:string?) as node()?
{
	if (empty($target)) then $node else
	if ($node/ancestor-or-self::*[
			(@data-exclude and contains-token(@data-exclude, $target)) or
			(@data-include and not(contains-token(@data-include, $target)))]) then
		()
	else
		$node
};

declare function _:lang($node as document-node()) as xs:string*
{
	$node//@*:lang/string()
};

declare function _:id($node as node()) as xs:string
{
	if ($node/@id) then $node/@id/string() else
	try {
		db:name($node) || "-" || db:node-id($node)
	}
	catch * {
		random:uuid()
	}
};

declare function _:root($node as node(), $roles as element(role)*) as node()?
{
	$node/ancestor-or-self::*:section[@role = $roles[type="section"]/id][last()]
};

declare function _:root($node as node()) as node()?
{
	_:root($node, roles:book(root($node)))
};

declare function _:serialize($path, $item as item()?)
{
	switch(_:mime-type($path))
		case "application/json" return
			serialize($item, map {"method": "json"})
		case "text/html" return
			serialize($item, map {"method" : "html"})
		case "application/xhtml+xml" return
			serialize($item, map {"method": "xhtml", "omit-xml-declaration": "no"})
		case "text/plain" return
			serialize($item, map {"method": "text"})
		case "text/comma-separated-values" return
			serialize($item, map {"method": "csv"})
		case "application/xml" return
			serialize($item, map {"method": "xml"})
		default return
			serialize($item, map {"method": "raw"})
};

declare function _:retrieve($uri as xs:string)
{
	if (starts-with($uri, "basex:"))
	then
		let $path := substring-after($uri, ":")
		let $db := substring-before($path, "/")
		let $file := substring-after($path, "/")
		return
			if (db:is-raw($db, $path))
			then db:retrieve($db, $path)
			else _:serialize($path, db:open($db, $path))
	else fetch:binary($uri)
};

declare function _:base-uri($path as xs:string)
{
	if (matches($path, "^\w+:")) then $path else "basex:" || $path
};

declare function _:filename($path as xs:string) as xs:string
{
	replace(file:name($path), "\.[*\.]*$", "")
};

declare function _:expand($elements as element()*, $names as xs:string*) as element()*
{
	for $el in $elements return
		if (local-name($el) = $names)
		then _:expand($el/*, $names)
		else $el
};


declare function _:join($items as item()*, $glue as xs:string) as xs:string
{
	string-join($items ! normalize-space(.) ! (if (.) then . else ()), $glue)
};

(:~
 : Add one or more classes to an element
 :)
declare updating function _:add-class($el as element(), $class as xs:string*)
{
	replace value of node $el/@class with
	string-join(
		distinct-values((
			tokenize($el/@class, "\p{Z}+"),
			tokenize($class, "\p{Z}+")
		)), " ")
};

