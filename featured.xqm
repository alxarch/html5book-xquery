module namespace _ = "html5book/featured";

import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";
import module namespace title = "html5book/title" at "title.xqm";
import module namespace label = "html5book/label" at "label.xqm";
import module namespace excerpt = "html5book/excerpt" at "excerpt.xqm";
import module namespace roles = "html5book/roles" at "roles.xqm";

declare function _:find($node as node()) as element(featured)*
{
	_:find($node, map {})
};

declare function _:find($node as node(), $options as map(*)) as element(asset)*
{
	let $roles :=
		if (map:contains($options, "roles"))
		then map:get($options, "roles")
		else roles:book(root($node))
	let $target := map:get($options, "target")
	for $f in $node//*[@data-featured and utils:filter(., $target)]
		let $id := head(($f/@data-featured-id/data(), $f/@id/data(), random:uuid()))
		group by $id
		let $type := head($f/@data-featured/data())
		let $html := <_>{$f/node()}</_>
		let $label := label:make($html)
		let $title := if ($f/@title) then head($f/@title) else title:make($html)
		let $location := utils:location(head($f))
		return
		<featured>
			<id>{$id}</id>
			<type>{$type}</type>
			<title>{$title}</title>
			{$location}
			<excerpt>{ excerpt:make($f/node(), 320) }</excerpt>
		</featured>
};

declare function _:to-json($feat as element(featured)) as element(json)
{
	copy $json := <json type="object" objects="location">{$feat/*}</json>
	modify (
		for $e in $json/excerpt return
			replace value of node $e with html5:stringify($e/node())
	)
	return $json
};

declare function _:to-json-array($feat as element(featured)*) as element(json)
{
	copy $json := <json type="array" objects="_ location">{$feat}</json>
	modify (
		for $e in $json/featured/excerpt return
			replace value of node $e with html5:stringify($e/node()),
		for $f in $json/featured return
			rename node $f as "_"
	)
	return $json
};

declare function _:count($book as node()) as element(featured-content)?
{
	let $feat := $book//*[@data-featured]
	return if ($feat) then
	<featured-content type="object">
	{
		for $f in $feat
			let $type := $f/@data-featured/string()
			group by $type
			return element {$type} {
				attribute type {"number"},
				let $fff := for $ff in $f
					let $first := head($ff)
					let $id := head(($ff/@data-featured-id, $ff/@id, db:node-id($first)))
					group by $id
					return $first

				return count($fff)
				}
	}
	</featured-content>
	else ()
};
