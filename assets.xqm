module namespace _ = "html5book/assets";

import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";
import module namespace roles = "html5book/roles" at "roles.xqm";
import module namespace title = "html5book/title" at "title.xqm";
import module namespace label = "html5book/label" at "label.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";

declare function _:make($a as node(), $roles as element(role)*) as element(asset)
{
	let $role := $a/@role/string()
	return
	<asset>
		<id>{$a/@id/data()}</id>
		<type>{$role}</type>
		<label>{ label:make($a) }</label>
		<title>{ title:make($a) }</title>
		{ utils:location($a, $roles) }
	</asset>
};

declare function _:find($node as node()) as element(asset)*
{
	_:find($node, map {})
};

declare function _:find($node as node(), $options as map(*)) as element(asset)*
{
	let $roles :=
		if (map:contains($options, "roles"))
		then map:get($options, "roles")
		else roles:book(root($node))
	let $asset-roles := $roles[type="asset"]/id/string()
	let $target := map:get($options, "target")
	let $asset-nodes :=
		if ($target)
		then $node//*[@role = $asset-roles and utils:filter(., $target)]
		else $node//*[@role = $asset-roles]
	return $asset-nodes ! _:make(., $roles)
};

declare function _:to-json-array($assets as element(asset)*) as element(json)
{
	copy $json := <json type="array" objects="_ location">{$assets}</json>
	modify (
		for $title in $json/asset/title return
			replace value of node $title with html5:stringify($title/node()),
		for $a in $json/asset return
			rename node $a as "_"
	)
	return $json
};

declare function _:count($book as node(), $roles as element(role)*) as element(assets)*
{
	let $assets := $book//*[@role = $roles[type="asset"]/id]
	return if ($assets) then
	<assets type="object">
	{
		for $a in $assets
			let $role := $a/@role/string()
			group by $role
			return element {$role} {attribute type {"number"}, count($a)}
	}
	</assets>
	else ()
};
