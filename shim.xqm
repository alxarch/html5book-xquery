module namespace _ = "html5book/shim";
(:~
 : Shim module to create fallbacks for missing functionality.
 :
 : Various end environments lack the ability to handle the full
 : spectrum of the HTML5 spec used in html5book. We provide here 
 : fallbacks for common cases.
 :)

import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";
import module namespace outline = "html5book/outline" at "outline.xqm";

(:~
 : Adds
 :)
declare updating function _:add-class($el as element(), $class as xs:string*)
{
	replace value of node $el/@class with
	string-join(
		distinct-values((
			tokenize($el/@class, "\p{Z}+"),
			tokenize($class, "\p{Z}+")
		)), " ")
};

(:~
 : Rename an element adding the tagname as classname.
 :
 :)
declare updating function _:rename($node as node(), $name-map as map(*))
{
	_:rename($node, $name-map, "")
};

(:~
 : Rename an element adding the tagname as classname.
 :)
declare updating function _:rename($node as node(), $name-map as map(*), $prefix as xs:string)
{
	for $el in $node//*[local-name() = map:keys($name-map)]
		let $name := local-name($el)
		return
		(
			rename node $el as QName(namespace-uri($el), $name-map($name)),
			_:add-class($el, $prefix || $name)
		)
};

(:~
 : Remove an attribute and add it as a class.
 :)
declare updating function _:attribute-as-class($node as node(), $names as xs:string*)
{
	_:attribute-as-class($node, $names, "")
};

declare updating function _:attribute-as-class($node as node(), $names as xs:string*, $prefix as xs:string)
{
	for $attr in $node//@*[local-name() = $names]
		let $el := $attr/..
		let $class := $prefix || local-name($attr)
		let $data := try {xs:Name($attr)} catch * { () }
		return (
			_:add-class($el, $class),
			if ($data) then _:add-class($el, $class || "-" || $data) else ()
		)
};

(:~
 : Replace a node with it's contents.
 :)
declare updating function _:explode($node as node(), $names as xs:string*)
{
	for $el in $node//*[local-name() = $names] return
		replace node $el with $node/node()
};

(:~
 : Remove data-attributes and try to add them as a class.
 : 
 : In case the content of the attribute is not a valid class name,
 : the data will be added in a comment of the form:
 : <!--data-foo=bar foo bar-->
 : as the first node inside the element or (in the case the element is
 : an HTML empty element as a comment:
 : <!--?data-foo=bar foo bar-->
 : as the first node *after* the element
 :)
declare updating function _:no-data-attributes($node as node()) {
	for $attr in $node//@*[starts-with(local-name(), "data-")]
		let $name := local-name($attr)
		let $data := $attr/data()
		let $el := $attr/..
		return
		(
			delete node $attr,
			_:add-class($el, $name),
			if ($data) then
				try { _:add-class($el, $name || ":" || xs:Name($data)) } catch * {
					if (html5:empty($el))
					then insert node comment { "?" || $name || "=" || $data } after $el 
					else insert node comment { $name || "=" || $data } as first into $el 
				}
			else ()
		)
};

(:~
 : Convert HTML5 <data> element to span
 : 
 : The value will be added in a comment of the form:
 : <!--value=bar foo bar-->
 : as the first node inside the element.
 :)
declare updating function _:convert-data($node as node())
{
	for $el in $node//*:data return
		(
			rename node $el as "span",
			delete node $el/@value,
			insert node comment {"value=" || $el/@value} as first into $el
		)
};

(:~
 : Remove microdata attributes.
 :)
declare variable $_:microdata-attr as xs:Name* := (
	"itemid", "itemprop", "itemref", "itemscope", "itemtype"
);

declare updating function _:no-microdata($node as node()) {
	delete node $node//@*[local-name() = $_:microdata-attr]
};


(:~
 : Attributes to keep only if the element matches.
 :)
declare variable $_:html4-attr-el-keep as map(*) := map {
	"tabindex": ("a", "area", "button", "object", "select", "textarea"),
	"accesskey": ("a", "area", "button", "input", "label", "legend", "textarea")
};

(:~
 : Global attributes are not so global in HTML 4
 :)
declare variable $_:html4-attr-el-remove as map(*) := map {
	"class": ("base", "basefont", "head", "html", "meta", "param", "script", "style", "title"),
	"style": ("base", "basefont", "head", "html", "meta", "param", "script", "style", "title"),
	"dir": ("applet", "base", "basefont", "bdo", "br", "frame", "frameset", "iframe", "param", "script"),
	"id": ("base", "head", "html", "meta", "script", "style", "title"),
	"lang": ("applet", "base", "basefont", "br", "frame", "frameset", "iframe", "param", "script"),
	"title": ("base", "basefont", "head", "html", "meta", "param", "script", "title")
};

declare variable $_:html4-attr-as-class as xs:Name* := (
	"role", "rel"
);

(:~
 : HTML 4.01 is very picky about attributes, this scrub will
 : help validators digest your markup.
 :)
declare updating function _:html4-attributes($node as node())
{
	(
		_:attribute-as-class($node, $_:html4-attr-as-class),
		_:no-microdata($node),
		_:no-start-attribute($node),
		_:no-data-attributes($node),
		delete node (
			for $attr in $node//attribute()[local-name() = map:keys($_:html4-attr-el-remove)]
				let $name := local-name($attr)
				let $el := local-name($attr/..)
				return
				if ($el = $_:html4-attr-el-remove($name))
				then $attr
				else (),
			for $attr in $node//attribute()[local-name() = map:keys($_:html4-attr-el-keep)]
				let $name := local-name($attr)
				let $el := local-name($attr/..)
				return
				if ($el = $_:html4-attr-el-keep($name))
				then ()
				else $attr
		)
	)
};

(:~
 : Remove audio, video elements.
 :)
declare updating function _:no-media($node as node())
{
	delete node $node//*[local-name() = ("audio", "video")]
};

(:~
 : Converts from html5 outline back to html4 outline.
 :)
declare updating function _:html4-outline($node as node())
{
	(
		for $el in $node//*[local-name() = $outline:sectioning-root-elements] return
			_:rename($el, map {
				"h1" : "div",
				"h2" : "div",
				"h3" : "div",
				"h4" : "div",
				"h5" : "div",
				"h6" : "div"
			}),
		for $el in $node//*[local-name() = $outline:heading-elements]
			let $level := outline:html4-heading-level($el)
			return if ($level le 0) then () else
			let $ns := namespace-uri($el)
			return
			if ($level gt 6)
			then (
				_:add-class($el, "el-h" || xs:string($level)),
				rename node $el as QName($ns, "div")
				)
			else
				rename node $el as QName($ns, "h" || xs:string($level)),
		_:rename($node, map {
				"article" : "div",
				"section" : "div",
				"header"  : "div",
				"footer"  : "div",
				"aside"   : "div"
			})
	)
};

(:~
 : Shim for the <ol>'s @start attribute.
 :)
declare updating function _:no-start-attribute($node as node())
{
	for $ol in $node//*:ol[@start] return
		for $i in 1 to $ol/@start return
			insert node element {QName(namespace-uri($ol), "li")} {
				attribute style { "visibility: hidden; height: 0;" }
			} as first into $ol
};

(:~
 : Umbrella function for all shims.
 :
 : TODO: handle order of updates properly.
 :)
declare updating function _:html($node as node(), $options as map(*))
{
	(
		for $key in map:keys($options)
			let $value := $options($key)
			let $html4 := map:get($options, "html4-attributes") eq "yes"
			return
			switch ($key)
				case "html4-outline" return
					if ($value = "yes")
					then _:html4-outline($node)
					else ()
				case "data-attributes" return
					if ($value = "no" and not($html4))
					then _:no-data-attributes($node)
					else ()
				case "microdata" return
					if ($value = "no" and not($html4))
					then _:no-microdata($node)
					else ()
				case "media" return
					if ($value = "no")
					then _:no-media($node)
					else ()
				case "start-attribute" return
					if ($value = "no" and not($html4))
					then _:no-start-attribute($node)
					else ()
				case "html4-attributes" return
					if ($value = "yes")
					then _:html4-attributes($node)
					else ()
				default return ()
	)
};
