module namespace _ = "html5book/cache";

declare variable $_:dir := db:system()/globaloptions/webpath/string() || "/__cache__/";

declare function _:not-found($path as xs:string)
{
	error(xs:QName("_:not-found"), "Path " || $path || " was not found in cache.")
};

declare function _:path($path as xs:string) as xs:string
{
	string-join((replace($_:dir, "/+$", ""), replace($path, "^/+", "")), "/")
};

declare function _:exists($path as xs:string) as xs:boolean
{
	false()
};

declare function _:retrieve($path as xs:string) as xs:base64Binary
{
	try {
		file:read-binary(_:path($path))
	}
	catch * {
		_:not-found($path)
	}
};

declare function _:store($path as xs:string, $data as xs:base64Binary)
{
	file:write-binary(_:path($path), $data)
};
