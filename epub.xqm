module namespace _ = "html5book/epub";

import module namespace contents = "html5book/contents" at "contents.xqm";
import module namespace meta = "html5book/meta" at "meta.xqm";
import module namespace media = "html5book/media" at "media.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";
import module namespace html = "html5book/epub/html" at "epub/html.xqm";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace opf = "http://www.idpf.org/2007/opf";

(:~
 : Prefix to use for content
 :)
declare variable $_:prefix as xs:string := "OEPBS/";

declare variable $_:defaults := map {
	"target": "epub",
	"version": 3.0,
	"ibooks": false()
};

(:~
 : Wrapper function for html5book/contents:book()
 : 
 : Enforces "epub" target.
 : Injects <href> containing the proper epub href for the item.
 :)
declare function _:contents($book as document-node(), $options as map(*)) as element(item)*
{
	copy $co := <_>{contents:book($book, map:merge(($_:defaults, $options)))}</_>
	modify (
		for $item in $co//*:item
			let $id := $item/id/string()
			let $root := $item/ancestor-or-self::item[last()]/id/string()
			let $href := element href {
					if ($id eq $root)
					then $id || ".xhtml"
					else $root || ".xhtml#" || $id
				}
			return insert node $href into $item
	)
	return $co/*
};

(:~
 : Create a map of usefull epub metadata.
 :)
declare function _:metadata($book as document-node()) as map(*)
{
	let $meta := meta:book($book)
	let $now := xs:string(current-dateTime())
	let $lang := utils:lang($book)
	return
	map {
		"id" : normalize-space(head((
			$meta/identifier/isbn/epub,
			$meta/identifier/isbn/print,
			$meta/identifier/pkgid))),
		"title": utils:join($meta/(title|subtitle), " - "),
		"creator": utils:join($meta/author, ", "),
		"cover": $meta/cover/href/string(),
		"cover-id": _:path-to-id($meta/cover/href),
		"publisher": normalize-space($meta/publisher),
		"pubdate": substring-before($now, "T"),
		"modified": substring-before($now, ".") || "Z",
		"copyright": utils:join($meta/copyright, " - "),
		"contributor": utils:join($meta/credits/translation, ", "),
		"ibooks-version": $meta/ibooksver/data(),
		"lang": if (empty($lang)) then ("el", "en") else $lang
	}
};

(:~
 : File uri for epub's skel dir.
 :)
declare variable $_:skel := resolve-uri("epub/skel/", static-base-uri());

(:~
 : Build a map containing mime-types for skel/ files.
 :)
declare function _:files($book as document-node(), $options as map(*)) as map(*)
{
	map:merge((
		file:list($_:skel, true()) ! map:entry(., utils:mime-type(.)) 
	))
};

(:~
 : Generate a valid id value from a path.
 :)
declare function _:path-to-id($path as xs:string) as xs:string
{
	let $path := replace($path, ' ', '_')
	let $path := replace($path, '/', '_')
	let $path := replace($path, '\.', '-')
	return $path
};


(:~
 : File uri for the content.opf template.
 :)
declare %private variable $_:opf-uri := resolve-uri("epub/opf.xql", static-base-uri());

(:~
 : Generates a content.opf
 :)
declare function _:opf($book as document-node(), $options as map(*))
{
	xquery:invoke($_:opf-uri, map {
		"book" : $book,
		"options" : _:options($options)
	})
};

(:~
 : File uri for the nav.xhtml template.
 :)
declare %private variable $_:nav-uri := resolve-uri("epub/nav.xql", static-base-uri());


(:~
 : Generates a nav.xhtml.
 :)
declare function _:nav($book as document-node(), $options as map(*))
{
	let $opt := _:options($options)
	let $nav := xquery:invoke($_:nav-uri, map {
		"book" : $book,
		"options" : $opt
	})
	return _:render($book, $nav, $opt)
};

(:~
 : Finds appropriate version number from type.
 :)
declare function _:version($type as xs:string) as xs:decimal
{
	switch ($type)
		case "3"
		case "epub3"
		case "ibooks"
			return 3.0
		case "epub2"
		case "2"
			return 2.0
		default
			return 2.0
};	

(:~
 : File uri for the toc.ncx template.
 :)
declare %private variable $_:ncx-uri := resolve-uri("epub/ncx.xql", static-base-uri());

(:~
 : Generate a toc.ncx file.
 :)
declare function _:ncx($book as document-node(), $options as map(*))
{
	xquery:invoke($_:ncx-uri, map {
		"book" : $book,
		"options" : _:options($options)
	})
};

(:~
 : Normalize option map.
 :)
declare function _:options($options as map(*)) as map(*)
{
	let $opt := map:merge(($_:defaults, $options))
	let $version := if ($opt("ibooks")) then 3.0 else xs:decimal($opt("version"))
	return map:merge(($opt, map {
		"version" : $version,
		"epub2" : floor($version) eq 2,
		"epub3" : floor($version) eq 3
		}))
};

declare %private function _:entry($path, $data) {
	map:entry($_:prefix || $path, $data)
};

declare variable $_:template-uri := resolve-uri("epub/template.xql", static-base-uri());

declare function _:render($book as document-node(), $html as item()*, $options as map(*)) as node()*
{
	let $opt := _:options($options)
	let $html :=
		if ($opt("epub3"))
		then html:epub3($book, $html)
		else html:epub2($book, $html)
	return
	xquery:invoke($_:template-uri, map {
		"options": $options,
		"metadata": _:metadata($book),
		"html": $html
		})
};

(:~
 : Serialization parameters for epub 2 output.
 :)
declare variable $_:epub2-serialization-params as map(*) := map {
	"method": "xhtml",
	"version": "1.1",
	"omit-xml-declaration": "no",
	"doctype-system": "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd",
	"doctype-public": "-//W3C//DTD XHTML 1.1//EN"
};

(:~
 : Serialization parameters for epub 3 output.
 :)
declare variable $_:epub3-serialization-params as map(*) := map {
	"method": "xhtml",
	"html-version": "5.0",
	"omit-xml-declaration": "no"
};

(:~
 : Builds an epub archive.
 :)
declare function _:build($book as document-node(), $options as map(*))
{
	let $opt := _:options($options)
	let $metadata := _:metadata($book)
	let $ncx := _:ncx($book, $opt)
	let $opf := _:opf($book, $opt)
	let $media := media:find($book, $opt)
	let $contents := _:contents($book, map {"max-depth": 1})
	let $serialize-xml := serialize(?, map {"method": "xml"})
	let $serialize-xhtml := serialize(?,
		if ($opt("epub3"))
		then $_:epub3-serialization-params
		else $_:epub2-serialization-params)
	let $pairs := map:merge((
		_:entry("content.opf", $serialize-xml($opf)),
		if ($opt("epub2")) then _:entry("toc.ncx", $serialize-xml($ncx)) else (),

		for $item in $contents
			let $id := $item/id/string()
			let $path := $id || ".xhtml"
			let $part := $book//*[@id=$id]
			let $doc := _:render($book, $part, $opt)
			let $data := $serialize-xhtml($doc)
			return _:entry($path, $data),

		file:list($_:skel, true()) ! _:entry(., file:read-binary($_:skel || .)),
		$media ! _:entry(src, utils:retrieve(resolve-uri(src, utils:base-uri(base-uri($book)))))))
		
	let $entries := map:keys($pairs)
	return archive:create($entries, $entries ! map:get($pairs, .))
};
