module namespace _ = "html5book/terms";

import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";
import module namespace roles = "html5book/roles" at "roles.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";

import module namespace title = "html5book/title" at "title.xqm";
import module namespace label = "html5book/label" at "label.xqm";

(:~
 : Dereference term references.
 :)
declare function _:ref-ids($nodes as node()*, $options as map(*)) as xs:string*
{
    _:refs($nodes, $options)/substring-after(@href, "#")
};


declare function _:refs($nodes as node()*, $id as xs:string, $options as map(*)) as node()*
{
	let $target := map:get($options, "target")
	return $nodes//*:a[@rel="term" and @href = "#" || $id and utils:filter(., $target)]
};

(:~
 : Find term references.
 :)
declare function _:refs($nodes as node()*, $options as map(*)) as node()*
{
	let $target := map:get($options, "target")
	return $nodes//*:a[@rel="term" and utils:filter(., $target)]
};

declare function _:title($node as node()) as node()*
{
	html5:normalize-space(title:html(head(($node/ancestor::*:dt, $node))/node()))
};

declare function _:find($node as node()) as element(term)*
{
	_:find($node, map {})
};


declare function _:find($node as node(), $options as map(*)) as element(term)*
{
	let $book := root($node)
	let $roles :=
		if (map:contains($options, "roles"))
		then map:get($options, "roles")
		else roles:book($book)
	let $refs := _:refs($book, $options)
	let $target := map:get($options, "target")
	return
	for $dfn in $node//*:dfn[utils:filter(., $target)] return
		<term type="object">
        	<id>{ $dfn/@id/data() }</id>
            <name> { _:defined-term($dfn) } </name>
            <title> { _:title($dfn) } </title>
            <definition> { _:define($dfn) } </definition>
            <refs type="array">
			{
            	for $r in $refs[@href = "#" || $dfn/@id] return
        		  <_ type="object">
                    {
                        if ($r/@id/data()) then <id>{$r/@id}</id> else (),
                        utils:location($r, $roles)
                    }
                </_>
            }
            </refs>
        </term>
};

declare function _:referenced($nodes as node()*, $terms as element(term)*) as element(term)*
{
	_:referenced($nodes, $terms, map {})
};

declare function _:referenced($nodes as node()*, $terms as element(term)*, $options as map(*)) as element(term)*
{
	_:ref-ids($nodes, $options) ! $terms[@id = .]
};

declare function _:define($term as item()) as node()*
{
    typeswitch($term)
      case node() return
		let $n := head((
			$term/ancestor-or-self::*:dt,
			$term/ancestor-or-self::*:dfn,
			$term))
		return switch(local-name($n))
			case "dt" return 
				let $next := head($n/following-sibling::*:dt)
				return
				$n/following-sibling::*:dd[. << $next]/node()
			case "dfn" return
				utils:closest($term, ("p", "section"))
			default return
				_:define($n/string())
      default return
        let $normalzed-term := normalize-space($term)
        return if ($normalzed-term = "") then () else
        let $root := root($term)
        return _:define(head($root//*:dfn[_:defined-term(.) = $normalzed-term]))
};

declare function _:defined-term($dfn as element()) as xs:string
{
    normalize-space(head((
        $dfn/@title,
        if ($dfn/*:abbr/@title and normalize-space($dfn) = normalize-space($dfn/*:abbr)) then
            $dfn/*:abbr/@title
        else
            (),
        $dfn
    )))
};

declare function _:to-json($term as element(term)) as element(json)
{
    copy $json := <json type="object" objects="location _" arrays="refs">{$term/node()}</json>
    modify (
        for $node in $json/(definition|title) return
            replace value of node $node with html5:stringify($node/node())
    )
    return $json
};

declare function _:to-json-array($terms as element(term)*) as element(json)
{
    copy $json := <json type="array">{$terms}</json>
    modify (
        for $node in $json/term/(definition|title) return
            replace value of node $node with html5:stringify($node/node()),
        for $term in $json/term return
            rename node $term as "_"
    )
    return $json
};
