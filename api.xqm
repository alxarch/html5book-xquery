module namespace _ = "html5book/api";

import module namespace req = "http://exquery.org/ns/request";

import module namespace contents = "html5book/contents" at "contents.xqm";
import module namespace outline = "html5book/outline" at "outline.xqm";
import module namespace feat = "html5book/featured" at "featured.xqm";
import module namespace assets = "html5book/assets" at "assets.xqm";
import module namespace terms = "html5book/terms" at "terms.xqm";
import module namespace meta = "html5book/meta" at "meta.xqm";
import module namespace me = "html5book/media" at "media.xqm";
import module namespace book = "html5book/book" at "book.xqm";
import module namespace u = "html5book/utils" at "utils.xqm";
import module namespace gm = "graphicsmagick" at "modules/graphicsmagick.xqm";
import module namespace cache = "html5book/cache" at "cache.xqm";

declare variable $_:params := map {
	"target" : req:parameter("target")
};

declare %rest:GET
		%rest:path("books/{$id}/contents.json")
		%output:method("json")
function _:contents($id as xs:string)
{
	contents:to-json(contents:book(book:open($id), $_:params))
};
declare %rest:GET
		%rest:path("books/{$id}/outline.json")
		%output:method("json")
function _:outline($id as xs:string)
{
	outline:to-json(outline:book(book:open($id), $_:params))
};

declare %rest:GET
		%rest:path("books/{$id}/assets.json")
		%output:method("json")
function _:assets($id as xs:string)
{
	assets:to-json-array(assets:find(book:open($id), $_:params))
};

declare %rest:GET
		%rest:path("books/{$id}/meta.json")
		%output:method("json")
function _:meta($id as xs:string)
{
	meta:to-json(meta:book(book:open($id)))
};

declare %rest:GET
		%rest:path("books/{$id}/terms.json")
		%output:method("json")
function _:terms($id as xs:string)
{
	terms:to-json-array(terms:find(book:open($id), $_:params))
};

declare %rest:GET
		%rest:path("books/{$id}/featured.json")
		%output:method("json")
function _:featured($id as xs:string)
{
	feat:to-json-array(feat:find(book:open($id), $_:params))
};

declare %rest:GET
		%rest:path("books/{$id}/media.json")
		%output:method("json")
function _:media($id as xs:string)
{
	me:to-json-array(me:find(book:open($id), $_:params))
};

declare %rest:GET
		%rest:path("books.json")
		%output:method("json")
function _:books()
{
	book:to-json-array(book:list())
};


declare %rest:GET
		%rest:path("books/{$id}/files/{$path=.+}")
function _:files($id as xs:string, $path as xs:string)
{
	let $db := "book-" || $id
	let $exists := db:exists($db, $path)
	return if (not($exists)) then _:not-found("File not found.") else
	let $raw := db:is-raw($db, $path)
	return
	(
    <rest:response>
      <http:response>
        <http:header name="Cache-Control" value="max-age=3600,public"/>
      </http:response>
      <output:serialization-parameters>
        <output:media-type value="{db:content-type($db, $path)}"/>
        <output:method value="{if ($raw) then "raw" else "adaptive"}"/>
      </output:serialization-parameters>
    </rest:response>,
	if ($raw) then db:retrieve($db, $path) else db:open($db, $path)
	)
};

(:~
 : Catches not-found errors for file, db, fetch modules
 :)
declare %rest:error("*:not-found", "*:BXDB0002", "*:BXFE0001")
		%rest:error-param("description", "{$msg}")
		%rest:produces("text/plain")
		%output:method("text")
function _:not-found($msg)
{
	_:error(404, $msg)
};

declare function _:error($status as xs:integer, $msg as xs:string)
{
	<rest:response>
		<output:serialization-parameters>
			<output:media-type value="text/plain"/>
			<output:method value="text"/>
		</output:serialization-parameters>
		<http:response status="{$status}" message="{$msg}">
			<http:header name="Content-Type" value="text/plain; charset=utf-8"/>
		</http:response>
	</rest:response>
};

declare %rest:GET
		%rest:path("books/{$id}/archive.zip")
function _:archive($id as xs:string)
{
	(
    <rest:response>
      <http:response>
        <http:header name="Content-Disposition" value="attachment; filename=book-{$id}-{u:timestamp("sec")}.zip"/>
        <http:header name="Content-Type" value="application/zip"/>
      </http:response>
      <output:serialization-parameters>
        <output:method value='raw'/>
      </output:serialization-parameters>
    </rest:response>,
	book:zipball($id)
	)
};

declare function _:file-response($file as xs:string)
{
    (<rest:response>
      <http:response>
        <http:header name="Cache-Control" value="max-age=3600,public"/>
      </http:response>
      <output:serialization-parameters>
        <output:media-type value="{u:mime-type($file)}"/>
		<output:method value="raw"/>
      </output:serialization-parameters>
    </rest:response>,
	file:read-binary($file))
};

declare %rest:GET
		%rest:path("books/{$id}/img/{$size}/{$path=.+}")
		%output:method("raw")
function _:img($id as xs:string, $size as xs:string, $path as xs:string)
{
	let $thumb := cache:path(string-join(("img", "book-" || $id, $size, $path), "/"))
	let $mime := u:mime-type($thumb)
	return
	if (not(starts-with($mime, "image/"))) then _:error(400, "Resource is not an image.")
	else if (file:exists($thumb)) then _:file-response($thumb) else
	let $img := string-join((
		db:system()/globaloptions/dbpath, 
		"book-" || $id,
		"raw",
		$path), "/")
	return if (file:exists($img)) then
		(
			gm:thumbnail($img, $thumb, $size),
			_:file-response($thumb)
		)
	else
		_:not-found("Image  " || $path || " was not found.")
};

declare %rest:POST("{$json}")
		%rest:path("books")
		%output:method("html")
		%output:version("5.0")
		%rest:consumes("application/json", "text/json")
function _:create-book($json as element(json))
{
	let $meta := meta:from-json($json)
	return
	<html>
		<head>
			<title>{$json/title/text()}</title>
			{$meta}
		</head>
		<body>
		</body>
	</html>
};
