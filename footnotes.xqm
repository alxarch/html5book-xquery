module namespace _ = "html5book/footnotes";

declare variable $_:default-symbols := ('*', '†', '‡', '§', '‖', '¶');

declare updating function _:fix($html as node())
{
	_:fix($html, $_:default-symbols)
};

declare updating function _:fix($html as node(), $symbols as xs:string+)
{
	let $total := count($symbols)
	let $root := root($html)
	return
	(
		for $note in $root//*[@role="footnote" and @data-symbol and @id] return
			(
				insert node element {QName(namespace-uri($note), "sup")} {
					text {$note/@data-symbol}
				} as first into $note,

				for $ref in $root//*:a[@href = "#" || $note/@id] return
					replace value of node $ref with $note/@data-symbol/string()
			),
		for $fn at $i in $html//*:a[@rel="footnote" and normalize-space(.) eq ""]
			let $repeat := ($i idiv $total) + 1
			let $symbol := $symbols[($i mod $total) + 1]
			let $txt := string-join((1 to $repeat) ! $symbol)
			return
			insert node element text {$txt} as first into $fn
	)
};
