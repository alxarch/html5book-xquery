module namespace _ = "html5book/book";

import module namespace meta = "html5book/meta" at "meta.xqm";
import module namespace assets = "html5book/assets" at "assets.xqm";
import module namespace feat = "html5book/featured" at "featured.xqm";
import module namespace roles = "html5book/roles" at "roles.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";

declare variable $_:not-found-error := xs:QName("_:not-found");

declare function _:not-found($id as xs:string) {
	error($_:not-found-error, "Book " || $id || " was not found.")
};

declare function _:open($id as xs:string) as document-node()?
{
	db:open(_:check($id), "book.xhtml")
};

declare function _:list() as element(book)*
{
	for $db in db:list()[starts-with(., "book-")]
		where db:exists($db, "book.xhtml")
		let $id := replace($db, "^book-", "")
		let $book := db:open($db, "book.xhtml")
		let $meta := meta:book($book)
		let $roles := roles:book($book)
		return
		<book type="object">
			<id>{$id}</id>
			{meta:to-json($meta)/*}
			{_:has($book, $roles)}
		</book>
};

declare function _:head($book as document-node()) as element()?
{
	$book/*:html/*:head
};
declare function _:has-slides($book as node()) as xs:boolean
{
	let $link := _:head($book)/*:link[@rel= $meta:prefix || ".slides"]
	return $link and db:exists(db:name($book), $link/@href)
};

declare function _:has($book as node(), $roles as element(role)*) as element(has)
{
	<has type="object">
		{feat:count($book)}
		{assets:count($book, $roles)}
		{ if (_:has-slides($book)) then <slides type="boolean">true</slides> else ()}
	</has>
};

declare function _:to-json-array($books as element(book)*) as element(json)
{
	copy $json := <json type="array">{$books}</json>
	modify for $b in $json/book return rename node $b as "_"
	return $json
};

declare function _:check($id as xs:string) as xs:string
{
	let $db := "book-" || $id
	return if (db:exists($db)) then $db else _:not-found($id)
};

declare function _:exists($id as xs:string) as xs:boolean
{
	db:exists("book-" || $id, "book.xhtml")
};

declare function _:zipball($id as xs:string) as xs:base64Binary
{
	let $db := _:check($id)
	let $prefix := "book-"||$id||"-zipball"
	let $tmp := file:create-temp-dir($prefix, "")
	return head((
		db:export($db, $tmp),
		utils:zipball($tmp),
		file:delete($tmp, true())
	))
};
