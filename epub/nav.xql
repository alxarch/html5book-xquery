declare default element namespace "http://www.w3.org/1999/xhtml";

import module namespace epub = "html5book/epub" at "../epub.xqm";

declare variable $contents external;
declare variable $options external;

declare variable $toc-class external := "TableOfContents";
declare variable $toc-item-class external := "navtoc-item";
declare variable $toc-id external := "section_navtoc";
declare variable $toc-label external := "Περιεχόμενα";
declare variable $toc-el := if ($options("epub3")) then "nav" else "div";
declare variable $toc-type :=
	if ($options("epub3"))
	then attribute epub:type {"toc"}
	else ();

declare function local:title($item) as node()*
{
	(
		$item/*:label/text(),
		if (string($item/*:label) ne "" and string($item/*:title) ne "")
		then text {" - "}
		else (),
		$item/*:title/node()
	)
};

declare function local:ol($items as element()*)
{
	<ol>
		$items ! 
		<li class="{$toc-item-class}">
			<a href="{*:href}">{local:title(.)}</a>
			{
				if (*:contents/*:item)
				then local:ol(*:contents/*:item)
				else ()

			}
		</li>
	</ol>
};

<div class="{$toc-class}" id="{$toc-id}-section">
	{ $toc-type }
	<h1>{$toc-label}</h1>
	
	{
		element {$toc-el} {
			$toc-type,
			attribute class {"toc"},
			attribute id {$toc-id},
			local:ol($contents/*:item)
		}
	}
</div>
