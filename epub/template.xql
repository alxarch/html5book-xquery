declare default element namespace "http://www.w3.org/1999/xhtml";

declare variable $html as item()* external;
declare variable $metadata as map(*) external;
declare variable $options as map(*) external;

<html xmlns:epub="http://www.idpf.org/2007/ops">
	{
		head($metadata("lang")) ! attribute xml:lang {.}
	}
	<head>
		{
			if ($options("epub3"))
			then <meta charset="utf-8"/>
			else ()
		}
		<title>{$metadata("title")}</title>
		<link rel="stylesheet" href="css/epub.css"/>
	</head>
	<body>
		{$html}
	</body>
</html>
