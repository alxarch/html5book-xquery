declare default element namespace "http://www.daisy.org/z3986/2005/ncx/";

import module namespace epub = "html5book/epub" at "../epub.xqm";
import module namespace utils = "html5book/utils" at "../utils.xqm";

declare option output:method "xml";
declare option output:media-type "application/x-dtbncx+xml";

declare variable $book as document-node() external;

declare variable $depth as xs:integer external := 2;
declare variable $total-page-count as xs:integer external := 0;
declare variable $max-page-number as xs:integer external := 0;
declare variable $cover-label as xs:string external := "Εξώφυλλο";
declare variable $cover-path as xs:string external := "cover.xhtml";

declare variable $metadata as map(*) := epub:metadata($book);
declare variable $cover as xs:string := map:get($metadata, "cover");
declare variable $contents as element()* := epub:contents($book, map {
	"max-depth" : $depth
	});
declare variable $play-order-offset as xs:integer := if ($cover) then 1 else 0;

<ncx version="2005-1">
	<head>
		<meta name="dtb:uid" content="{$metadata("id")}"/>
		<meta name="dtb:depth" content="{$depth}"/>
		<meta name="dtb:totalPageCount" content="{$total-page-count}"/>
		<meta name="dtb:maxPageNumber" content="{$max-page-number}"/>
	</head>
	<docTitle>
		<text>{$metadata("title")}</text>
	</docTitle>
	<navMap>
	{
		if ($cover) then
			<navPoint id="cover" playOrder="0">
				<navLabel><text>{$cover-label}</text></navLabel>
				<content src="{$cover-path}"/>
			</navPoint>
		else
			()
	}
	{
		for $nav at $i in <_>{$contents}</_>//*:item return
			<navPoint id="{$nav/*:id}" playOrder="{$i + $play-order-offset}">
				<navLabel>
					<text>{ utils:join(($nav/*:label, $nav/*:title), " - ") }</text>
				</navLabel>
				<content src="{$nav/*:href}"/>
			</navPoint>
	}
	</navMap>
</ncx>
