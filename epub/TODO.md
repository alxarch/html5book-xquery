on init.coffee we write some css ("epub.css") in the head. Why? it has imports.

filter
	target = epub

auto-id a, section, +?

auto-rel

endnote/footnote mayhem
	data-symbol
	fill number/symbol
	back links?
	move footnotes to end of closest section/figure

	
epub shims
	wrap tables
	add alt attribute to images
	inline-css
		from epub/inline-css.json
	
	fix htmltags epub2
		html 4 attr?
		shim:no-start-attribute
		shim:html4-global-attributes

		wtf happens with attributes?

url-rewrite
	check mime
		media download
			use fetch module
			create reverse domain folders

list-of???

toc.ncx
content.opf
nav.xhtml  (what happens on epub2?)
cover.xhtml
mimetype uncompressed

