module namespace _ = "html5book/epub/html";
import module namespace utils = "html5book/utils" at "../utils.xqm";
declare namespace epub = "http://www.idpf.org/2007/opf";
declare variable $_:table-wrapper-class := "table-wrapper";

declare updating function _:fix-hrefs($book as document-node(), $html as node())
{
	for $a in $html//*:a[starts-with(@href, "#")]
		let $id := substring-after($a/@href, '#')
		let $target := $book//*[@id = $id]
		let $rel :=
			if ($target/@role = ("endnote", "footnote")) 
			then $target/@role/string()
			else if (local-name($target) eq "dfn")
			then "term"
			else "read"

		let $root := utils:root($target)
		let $root-id := utils:id($root)
		return (
			delete node $a/@rel,
			insert node attribute rel {$rel} into $a,
			replace value of node $a/@href with
			if ($root-id eq $id)
			then $id || ".xhtml"
			else $root-id || ".xhtml#" || $id
		)
};

declare updating function _:fix-tables($html as node())
{
	for $el in $html//*:table return
		replace node $el with
		element {QName(namespace-uri($el), "div")} {
			attribute class {$_:table-wrapper-class},
			$el/@*,
			$el/node()
		}
};

declare updating function _:fix-img($html as node())
{
	for $img in $html//*:img[@src and empty(@alt)] return
		insert node attribute alt {utils:filename($img/@src)} into $img
};

declare updating function _:fix-ids($html as node())
{
	for $el in $html//(*:a, *:section)[empty(@id)] return
			insert node attribute id {utils:id($el)} as first into $el
};

declare variable $_:epub2-element-map := map {
	"section":    "div",
	"aside":      "div",
	"blockquote": "div",
	"header":     "div",
	"footer":     "div",
	"figure":     "div",
	"figcaption": "div",
	"label":      "strong",
	"u":          "span"
};

declare updating function _:epub2-tags($html as node())
{
	for $el in $html//*[local-name(.) = map:keys($_:epub2-element-map)] return
		rename node $el as QName(namespace-uri($el), $_:epub2-element-map(local-name($el))) 
};




declare updating function _:epub2-attributes($html as node())
{
	for $el in $html//*[not(empty(@role))] return
		(
			delete node ($el/@role, $el/@class),
			insert node
			attribute class {string-join(($el/@role/string(), $el/@class/string()), " ")}
			into $el
		)
};


declare updating function _:popup-footnotes($book as node(), $html as node())
{
	for $el in $html//*:a[starts-with(@href, "#") and @rel= ("endnote", "footnote")]
		let $id := substring-after($el/@href, "#")
		let $target := $book//*[@id = $id]
		let $role := $target/@role/string()
		return if ($role = ("footnote", "endnote")) then
			let $popup :=
				copy $p := $target
				modify(
					insert node attribute epub:type {"footnote"} into $p,
					for $a in $p//*:a return
						replace node $a with $a/node(),
					for $attr in $p//@id return
						replace value of node $attr with "popup_" || $attr
				)
				return $p
			return (
				insert node $popup as last into $target/..,
				replace value of node $el/@href with "#popup_" || $id,
				insert node attribute epub:type {"noteref"} into $el
			)
		else ()

};


declare function _:epub3($book as document-node(), $nodes as node()*) as node()*
{
	()
};

declare function _:epub2($book as document-node(), $nodes as node()*) as node()*
{
	()
};
