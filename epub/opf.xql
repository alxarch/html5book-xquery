declare default element namespace "http://www.idpf.org/2007/opf";

declare namespace dc = "http://purl.org/dc/elements/1.1/";

import module namespace epub = "html5book/epub" at "../epub.xqm";

declare option output:method "xml";

declare variable $book as document-node() external;
declare variable $options as map(*) external;

declare variable $identifier-id as xs:string := head((map:get($options, "identifier-id"), "book-id"));
declare variable $cover-label as xs:string := head((map:get($options, "cover-label"), "Εξώφυλλο"));

declare variable $metadata as map(*) := epub:metadata($book);
declare variable $contents := epub:contents($book, map { "max-depth" : 1 });

declare variable $ibooks-prefix as attribute()? :=
	if ($options("ibooks") and map:contains($metadata, "ibooks-version"))
	then attribute prefix {
		"ibooks: http://vocabulary.itunes.apple.com/rdf/ibooks/vocabulary-extensions-1.0/"}
	else ();

<package unique-identifier="{$identifier-id}" version="{out:format("%.1f", $options("version"))}">
	{ $ibooks-prefix }
	{
		copy $m :=
			<metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
				<dc:title>{$metadata("title")}</dc:title>
				<dc:date>{$metadata("pubdate")}</dc:date>
				<dc:identifier id="{$identifier-id}">{$metadata("id")}</dc:identifier>
				<dc:creator>{$metadata("creator")}</dc:creator>
				{ $metadata("lang") ! <dc:language>{.}</dc:language> }
				<dc:rights>{$metadata("copyright")}</dc:rights>
				<dc:publisher>{$metadata("publisher")}</dc:publisher>
				<dc:contributor>{$metadata("contributor")}</dc:contributor>

				{	
					if ($ibooks-prefix)
					then <meta property="ibooks:version" content="{$metadata("ibooks-version")}"/>
					else ()
				}
				<meta name="cover" content="{$metadata("cover-id")}" />
				{
					if ($options("version") ge 3.0)
					then <meta property="dcterms:modified">{$metadata("modified")}</meta>
					else ()
				}

			</metadata>
		modify
			(: Remove all empty elements :)
			delete node (
				$m/dc:*[. = ""],
				$m/meta[@content=""]
			)
		return $m
	}
	<manifest>
		<item id="cover" href="cover.xhtml" media-type="application/xhtml+xml">{
			if ($options("epub3"))
			then attribute properties {"cover-image"}
			else ()
		}</item>
		{
			if ($options("epub2"))
			then <item id="ncx" href="toc.ncx" media-type="application/x-dtbncx+xml"/>
			else ()
		}
		{
			for $item in $contents return
				<item id="{$item/*:id}" href="{$item/*:href}" media-type="application/xhtml+xml"/>
		}
		{
			map:for-each(epub:files($book, $options), function ($path, $mime) {
				<item id="{epub:path-to-id($path)}" href="{$path}" media-type="{$mime}"/>
			})
		}
	</manifest>
	<spine>
	{ if ($options("epub2")) then attribute toc {"ncx"} else () }
	{
		for $item in $contents return
		<itemref idref="{$item/*:id}"/>
	}
	</spine>
	<guide>
		<reference type="start" title="{$cover-label}" href="cover.xhtml"/>
	</guide>
</package>
