module namespace _ = "html5book/epub/api";

import module namespace epub = "html5book/epub" at "../epub.xqm";
import module namespace book = "html5book/book" at "../book.xqm";

declare %rest:GET
		%rest:path("books/{$id}/epub/{$type}/content.opf")
function _:opf($id, $type as xs:string) {
	epub:opf(book:open($id), map {
			"version" : epub:version($type),
			"ibooks" : $type eq "ibooks"
		})
};

declare %rest:GET
		%rest:path("books/{$id}/epub/{$type}/toc.ncx")
function _:ncx($id, $type as xs:string) {
	epub:ncx(book:open($id), map {
			"version" : epub:version($type),
			"ibooks" : $type eq "ibooks"
		})
};

declare %rest:GET
		%rest:path("books/{$id}/epub/{$type}/index.xhtml")
function _:nav($id, $type as xs:string) {
	epub:nav(book:open($id), map {
			"version" : epub:version($type),
			"ibooks" : $type eq "ibooks"
		})
};

