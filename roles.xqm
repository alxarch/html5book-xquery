module namespace _ = "html5book/roles";

import module namespace meta = "html5book/meta" at "meta.xqm";

declare variable $_:uri := resolve-uri("roles.json", static-base-uri());

declare variable $_:default := _:load($_:uri);


(:~
 : Load roles from a json file.
 :)
declare function _:load($uri as xs:string) as element(role)* {
	let $json := 
		try {
			json:parse(fetch:text($uri))/json
		}
		catch * {
			<json/>
		}
	return _:from-json($json)
};


(:~
 : Convert json roles to role element.
 :)
declare function _:from-json($json as element(json)) as element(role)* {
	copy $roles := $json
	modify (
		for $role in $roles/* return
			(
				insert node <id>{local-name($role)}</id> as first into $role,
				rename node $role as "role"
			)
	)
	return $roles/*
};


(:~
 : Merge roles overwriting base with other.
 :)
declare function _:merge($base as element(role)*, $other as element(role)*) as element(role)* {
	map:for-each(map:merge((
		$base ! map:entry(id, .),
		$other ! map:entry(id, .))),
		function ($key, $role) { $role })
};


(:~
 : Find the roles for a book
 :)
declare function _:book($book as document-node()) as element(role)*
{
	let $links := $book/html:head//html:link[@rel= $meta:prefix || ".roles"]
	let $uris := $links/@href ! resolve-uri(., base-uri($book))
	return fold-left($uris, $_:default, function ($result, $uri) {	
		_:merge($result, _:load($uri))
	})
};
