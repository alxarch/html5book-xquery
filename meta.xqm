module namespace _ = "html5book/meta";

declare namespace html = "http://www.w3.org/1999/xhtml";

declare variable $_:prefix := "book";

declare function _:book($book as document-node()) as element(meta)
{
	let $head := $book/html:html/html:head
	let $db := try {db:name($book)} catch * { () }
	return _:normalize(
    <meta type="object">
    {
		if ($db) then <node>{$db || ":" || db:node-id($book)}</node> else (),
		<title>{$head/html:title/data()}</title>,

        for $m in $head/html:meta[starts-with(@name, $_:prefix || ".")]
            let $names := tokenize(substring-after($m/@name, $_:prefix || "."), "\.")
            let $contents := $m/@content/string()
            return
            fold-right($names, $contents, function($name, $result){
                element {$name} {$result}
            }),
        for $lnk in $head/html:link[starts-with(@rel, $_:prefix || ".")]
            let $names := tokenize(substring-after($lnk/@rel, $_:prefix || "."), "\.")
            let $contents := (
				attribute type {"link"},
				<href> { $lnk/@href/string() } </href>,
				if ($lnk/@type) then <type>{ $lnk/@type/string() }</type> else (),
				if ($lnk/@media) then <media>{ $lnk/@media/string() }</media> else ()
				)
            return
            fold-right($names, $contents, function($name, $result){
                element {$name} {$result}
            })
    }
    </meta>)
};


declare %private function _:normalize($meta as element(meta)) as element(meta)
{
	copy $result := $meta
	modify (

		(: Remove duplicate title :)
		delete node tail($result/title),
		for $a in $result/credits/author return
			(delete node $a, insert node $a into $result),
		for $e in $result/credits/editor return
			(delete node $e, insert node $e into $result)
	)
	return $result
};

declare %private function _:as-array($name as xs:string, $nodes as node()*) as node()*
{
	element {$name} {
		attribute type {"array"},
		for $n in $nodes return
			if ($n/*) then _:as-object("_", $n) else _:as-value("_", $n)
	}
};

declare %private function _:as-value($name as xs:string, $el as element()) as element()
{
	let $txt := $el/text()
	let $data := normalize-space(string-join($txt))
	return
	element {$name} {
		if (empty($txt) or $data eq 'null') then attribute type {'null'}
		else if (string(number($data)) ne 'NaN') then (attribute type {'number'}, $data)
		else if ($data = ('true', 'false')) then (attribute type {'boolean'}, $data)
		else $data
	}
};

declare %private function _:as-object($name as xs:string, $el as element()) as element()
{
	element {$name} {
		attribute type {"object"},
		if ($el/@type)
			then element ____type____ {$el/@type/data()}
			else (),
		_:as-json($el/*)
	}
};

declare %private function _:as-json($nodes as node()*) as node()*
{
	for $node in $nodes
		let $name := local-name($node)
		group by $name
		return
		typeswitch ($node)
			case element() return
				if ($name = ("author", "editor") or count($node) gt 1) then
					_:as-array($name, $node)
				else if ($node/*) then
					_:as-object($name, $node)
				else
					_:as-value($name, $node)
			default return
				()
};

declare function _:to-json($meta as element(meta)) as element(json)
{
	<json type="object">
	{ _:as-json($meta/node()) }
	</json>
};

declare function _:to-json-array($meta as element(meta)*) as element(json)
{
	<json type="array">{$meta ! <_ type="object">{ _:to-json(.)/node() }</_>}</json>
};

declare function _:from-json($json as element(json)) as element(meta)*
{
	copy $meta := $json
	modify rename node $meta as $_:prefix
	return
		for $item in innermost($meta//*)
			let $path := $item/ancestor-or-self::*/local-name()
			return element meta {
				attribute content {$item/text()},
				attribute name { string-join($path, '.') }
			}
};
