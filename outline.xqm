module namespace _ = "html5book/outline";

import module namespace title = "html5book/title" at "title.xqm";
import module namespace utils = "html5book/utils" at "utils.xqm";
import module namespace label = "html5book/label" at "label.xqm";
import module namespace html5 = "utils/html5" at "modules/utils/html5.xqm";

(:~
 : HTML Outliner implementing the infamous and elusive html5 outline algorithm.
 :
 : https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Sections_and_Outlines_of_an_HTML5_document
 : http://html5doctor.com/outlines/
 : http://www.paciellogroup.com/blog/2013/10/html5-document-outline/
 :
 : TODO: Add $options for target filtering
 : TODO: Add $options for html5 only sections
 : TODO: Add method to convert html4 outline to html5
 :)

declare variable $_:heading-elements := ("h1", "h2", "h3", "h4", "h5", "h6");
declare variable $_:expand-elements := ("div", "header", "footer", "main");
declare variable $_:section-elements := ("body", "section", "aside", "article");

(:~
 : Convert outline items to JSON.
 :)
declare function _:to-json($items as element(item)*) as element(json)
{
	copy $json := <json type="array">{$items}</json>
	modify (
		for $title in $json//title return
			replace value of node $title with html5:stringify($title/node()),
		delete node $json//item/*[. = ""],
		for $item in $json//item
			let $next := head($item/following-sibling::*)/id/string()
			let $prev := ($item/preceding-sibling::*)[last()]/id/string()
			return (
				if ($next) then insert node <next>{$next}</next> into $item else (),
				if ($prev) then insert node <prev>{$prev}</prev> into $item else (),
				rename node $item as "_"
			)
	)
	return $json
};

(:~
 : Find the heading rank of an element.
 :)
declare function _:rank($el as element()?) as xs:integer
{
  if (empty($el)) then 0 else
	switch (local-name($el))
		case "h1" return 6
		case "h2" return 5
		case "h3" return 4
		case "h4" return 3
		case "h5" return 2
		case "h6" return 1
		default return 0
};


(:~
 : Find outline items.
 :)
declare function _:items($el as element()?, $contents as element()*) as element(item)*
{
	if (empty($el)) then () else
	let $title := title:node-alt($el)
	let $contents := utils:expand($contents, $_:expand-elements)
	let $items := 
		for $child in $contents
			where $child ne $title
			let $name := local-name($child)
			return
			if($name = $_:section-elements) then
				_:items($child, $child/*)
			else if ($name = $_:heading-elements) then
				let $rank := _:rank($child)
				let $pre := $contents[. << $child] except $title
				let $last := $pre[local-name() = ($_:section-elements, $_:heading-elements)][last()]
				let $last-rank := _:rank($last)
				return if ($last-rank gt $rank) then () else
				let $next := $contents[. >> $child]
				let $stop := head($next[local-name() = $_:section-elements or _:rank(.) ge $rank])
				let $next := if (empty($stop)) then $next else $next[. << $stop]
				return _:items($child, $next)
			else ()
	return
	<item type="object">
		<id>{head(($el/@id, $title/@id))/data()}</id>
		<type>{head(($el/@role, $title/@role))/data()}</type>
		<label>{label:make($el)}</label>
		<title>{title:make-alt($el)}</title>
		{
			if (empty($items)) then () else <items type="array">{$items}</items>
		}
	</item>
};

(:~
 : Find outline items in a document.
 :)
declare function _:book($book as document-node(), $options as map(*)) as element(item)*
{
	let $body := head($book//*:body)
	return _:items($body, $body/*)
};

declare variable $_:sectioning-root-elements as xs:Name* := ("figure", "blockquote", "fieldset", "details", "td");

(:~
 : Detects the heading level for a heading in an html5 outline
 :)
declare function _:html4-heading-level($el as element()) as xs:integer
{
	if ($el/ancestor::*[local-name() = $_:sectioning-root-elements]) then 0 else
	let $rank := _:rank($el)
	return if ($rank eq 0) then $rank else
	let $ancestors := $el/ancestor::*[local-name() = $_:heading-elements]
	let $section := $ancestors[last()]
	let $contents := utils:expand($section/*, $_:expand-elements)
	let $headings := $contents[_:rank(.) ge $rank and . << $el]
	return count(tail($headings)) + count($ancestors)
};

(:~
 : Expand elements returning only explicit sections.
 :)
declare function _:expand-sections($elements as element()*) as element(*)
{
	for $el in $elements return
		let $name := local-name($el)
		return
		if ($name = $_:section-elements) then $el
		else if ($name = $_:expand-elements) then _:expand-sections($el/*) 
		else ()
};

(:~
 : Expand elements returning only headings.
 :)
declare function _:expand-headings($elements as element()*) as element()*
{
	for $el in $elements return
		let $name := local-name($el)
		return
		if ($name = $_:heading-elements) then $el
		else if ($name = $_:expand-elements) then _:expand-headings($el/*) 
		else ()
};

(:~
 : Find the closest explicit section element.
 :)
declare function _:explicit-section($node as node()) as element()?
{
	($node/ancestor-or-self::*[local-name() = $_:section-elements])[last()]
};


(:~
 : Return an array containing all ids of parent sections.
 :)
declare function _:location($node as node()?) as element(location)
{
	<location type="array">
		{
			for $el in _:ancestors($node)
				let $id :=
					if ($el/@id)
					then $el/@id/string()
					else head(_:expand-headings($el/*))/@id/string()
				return if ($id) then element {"_"} { $id } else ()
		}
	</location>
};

(:~
 : Find all ancestor sections.
 :)
declare function _:ancestors($node as node()?) as element()*
{
	if (empty($node)) then () else
	let $section := _:section-element($node)
	return (_:ancestors($section/..), $section)
};

(:~
 : Find the closest section (explicit or implicit).
 :)
declare function _:section-element($node as node()?) as element()?
{
	if (empty($node)) then () else
	let $explicit-section := _:explicit-section($node)
	return if ($explicit-section eq $node) then $explicit-section else
	let $headings := _:expand-headings($explicit-section/*)
	let $implicit := tail($headings)[. << $node][last()] 
	return if (empty($implicit)) then $explicit-section else
	let $last-section := _:expand-sections($explicit-section/*)[. << $node and . >> $implicit]
	return if (empty($last-section)) then $implicit
	else $explicit-section
};
