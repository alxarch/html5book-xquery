module namespace _ = "html5book/fix";

declare updating function _:auto-id($node as node(), $elements as xs:Name*)
{
	for $el in $node//*[local-name() = $elements and not(@id)] return
		(
			delete node $el/@id,
			insert node attribute id {generate-id($el)} into $el
		)
};

declare updating function _:url-rewrite($node as node(), $rewrite as function(node(), xs:string) as xs:string)
{
	for $href in $node//(@href|@src) return
		replace value of node $href with $rewrite($href/.., $href/string())
};

declare updating function _:auto-rel($node as node())
{
	let $root := root($node)
	return
	(
	for $el in $node//*[matches(@href, "^(https?|s?ftp):")] return
		(
			delete node $el/@rel,
			insert node attribute rel {"ext"} into $el
		),
	for $el in $node//*[starts-with(@href, "#") and empty(@rel)]
		let $id := substring-after($el/@href, "#")
		let $target := head($root//*[@id = $id])
		let $rel :=
			if ($target/@role = ("footnote", "endnote"))
			then $target/@role/string()
			else if ($target/self::*:dfn)
			then "term"
			else ()
		return
		if ($rel)
		then insert node attribute rel {$rel} into $el
		else ()
	)
};
